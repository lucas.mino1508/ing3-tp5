def validar_credenciales(usuario, contrasena):
    # Aquí puedes implementar la lógica de validación de credenciales según tus necesidades
    # Por ejemplo, puedes comparar las credenciales con una base de datos o un archivo de usuarios registrados
    
    # Supongamos que tenemos un diccionario con usuarios y contraseñas almacenados en memoria
    usuarios = {
        "usuario1": "contrasena1",
        "usuario2": "contrasena2",
        "usuario3": "contrasena3"
    }
    
    if usuario in usuarios and usuarios[usuario] == contrasena:
        return True
    else:
        return False

# Ejemplo de uso:
nombre_usuario = input("Ingrese su nombre de usuario: ")
password = input("Ingrese su contraseña: ")

if validar_credenciales(nombre_usuario, password):
    print("Credenciales válidas. Inicio de sesión exitoso.")
else:
    print("Credenciales inválidas. Inicio de sesión fallido.")
