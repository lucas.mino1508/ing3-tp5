import random
from captcha.image import ImageCaptcha

# Generar captcha
def generate_captcha():
    captcha_text = ''.join(random.choices('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', k=6))
    image = ImageCaptcha(width=200, height=100)
    image_data = image.generate(captcha_text)
    return captcha_text, image_data

# Verificar captcha
def verify_captcha(captcha_text, user_input):
    return captcha_text == user_input

# Ejemplo de uso
captcha_text, image_data = generate_captcha()
print("Captcha:", captcha_text)

# El usuario proporciona la entrada
user_input = input("Introduce el captcha mostrado: ")

# Verificar el captcha
if verify_captcha(captcha_text, user_input):
    print("Captcha válido. Usuario no es un robot.")
else:
    print("Captcha inválido. Usuario podría ser un robot.")
